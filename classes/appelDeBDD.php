<?php

class appelDeBDD{
    static function BDD() {
        require_once 'config/config.php';
        $bdd = new PDO("mysql:host=$host;dbname=$dbName", $user, $pass);
        return $bdd;
    }
    static function insertNom($nom,$prenom,$age) {
        $bdd = appelDeBDD::BDD();
        $req = $bdd->prepare("INSERT INTO `utilisateurs`(`nom`, `prenom`, `age`) VALUES (:nom,:prenom,:age)");
        $req->execute([
            'nom'=>$nom,
            'prenom'=>$prenom,
            'age'=>$age
        ]);
    }
    static function deleteUtilisateurs($id) {
        $bdd = appelDeBDD::BDD();
        $req = $bdd->prepare("DELETE FROM `utilisateurs` WHERE id = :id");
        $req->execute([
            'id'=>$id
        ]);
    }
}